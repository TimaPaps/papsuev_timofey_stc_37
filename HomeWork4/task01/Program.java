import java.util.Scanner;

/** 
 *Рекурсивная функция, проверяющая, является ли число степенью двойки
 *
 *@autor Timofey Papsuev
 *@version 1.0
 */

class Program {
	public static void main(String[] args) {

		if (powerOfTwo(enterNumber()) == true) {
			System.out.println("Entered number is a power of two!");
		} else {
			System.out.println("Entered number is NOT a power of two.");
		}
	}

	/**
	 *Функция - ввод числа пользователем больше 0
	 *@return возвращает введенное число
	 */
	public static int enterNumber() {
		Scanner scanner = new Scanner(System.in);
		int number;

		while (true) {
			System.out.println("Enter integer number > 0");
			number = scanner.nextInt();;
			
			if (number < 1) {
					System.out.println("Entered number " + number);
					continue;
				} else {
					break;
				}
		}	
		
		return number;	
	}

	/**
	 *Функция - проверяет, является ли число степенью двойки
	 *@return возвращает true, если число является степенью двойки,
	 *иначе возврвщвает false
	 *@param number число введенное пользователем
	 */
	public static boolean powerOfTwo(int number) {	

		if (number == 2 || number == 1) {
			return true;
		} else if (number % 2 != 0) {
			return false;
		}

		return powerOfTwo(number / 2);
	}		
}