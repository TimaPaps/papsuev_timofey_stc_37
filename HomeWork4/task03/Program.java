/**
 *Числа Фибоначчи с однократным вызовом рекурсии
 *
 *@autor Timofey Papsuev
 *@version 1.0
 */

class Program {

	public static final int COUNT = 3;
	public static final int PRE_PREVIOUS = 1;
	public static final int PREVIOUS = 1;
	public static final int RESULT = 0;

	public static void main(String[] args) {
		System.out.println(fibonacci(9));
	}

	/**
	 *Функция - поиск числа Фибоначчи
	 *@return возвращает элемент последовательности Фибоначчи
	 *@param number число, индекс последовательности
	 */
	public static int fibonacci(int number) {
		return fibonacci(number, COUNT, PRE_PREVIOUS, PREVIOUS, RESULT);
	}

	/**
	 *Функция - поиск числа Фибоначчи
	 *@return возвращает элемент последовательности Фибоначчи
	 *@param number число, индекс последовательности
	 *@param count счетчик, для перебора последовательности до числа,
	 *индекса последовательности
	 *@param prePrevious первое предыдущее число Фибоначчи
	 *@param previous второе предыдущее число Фибоначчи
	 *@param result результирующее число Фибоначчи
	 */
	public static int fibonacci(int number, 
								int count, 
								int prePrevious, 
								int previous, 
								int result) {

		if (number == 1 || number == 2) {
			result = 1;
			return result;
		}

		if (number >= count) {
			result = prePrevious + previous;
			prePrevious = previous;
			previous = result;
			count++;			
		} else {
			return result;
		}

		return fibonacci(number, count, prePrevious, previous, result);
	}
}