import java.util.Scanner;

/**
 *Бинарный поиск с использованием рекурсии
 *
 *@autor Timofey Papsuev
 *@version 1.0
 */

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter number");
		int numberForSearch = scanner.nextInt();
		int[] array = {1, 7, 22, 28, 31, 33, 43, 55, 87, 99};
		int index = binarySearch(array, numberForSearch);

		if (index == -1) {
			System.out.println("Not found");
		} else {
			System.out.println("Is found, index of element " + index);
		}
	}

	/**
	 *Функция - бинарный поиск элемента в массиве
	 *@return возвращает индекс элемента в массиве, если он найден 
	 *или -1, если не найден
	 *@param array массив чисел, в котором осуществляется поиск 
	 *@param element введенное число
	 */
	public static int binarySearch(int[] array, int element) {
		return binarySearch(array, element, 0, array.length - 1);
	}

	/**
	 *Функция - бинарный поиск элемента в массиве
	 *@return возвращает индекс элемента в массиве, если он найден
	 *или -1, если не найден
	 *@param array массив чисел, в котором осуществляется поиск 
	 *@param element введенное число
	 *@param left левая граница области поиска в массиве
	 *@param right правая граница области поиска в массиве
	 */
	public static int binarySearch(int[] array, 
								   int element, 
								   int left, 
								   int right) {
		int found = -1;

		if (left > right) {
			return found;
		}

		int middle = left + (right - left) / 2;

		if (array[middle] == element) {
			return found = middle;
		} else if (array[middle] < element) {
			left = middle + 1;
		} else if (array[middle] > element) {
			right = middle - 1;		
		} 

		return binarySearch(array, element, left, right);
	}
}