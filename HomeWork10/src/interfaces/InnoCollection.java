package interfaces;

/**
 * 13.03.2021 11:10
 * HomeWork10
 *
 * Интерфейс - задает основной список методов для списков ArrayList и LinkedList
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface InnoCollection {
    /**
     * Метод - добавляет элемент в коллекцию
     *
     * @param element - добавляемый элемент
     */
    void add(int element);

    /**
     * Метод - удаляет элемент из коллекции
     *
     * @param element - удаляемый элемент
     */
    void remove(int element);

    /**
     * Метод - проверяет наличие элемента в коллекции
     *
     * @param element - искомый элемент
     * @return - true, если элемент найден; false, если элемент не найден
     */
    boolean contains(int element);

    /**
     * Метод - показывает количество элементов коллекции
     *
     * @return - значение, равное количеству элементов
     */
    int size();

    /**
     * Метод - итератор коллекции
     *
     * @return - объект-итератор
     */
    InnoIterator iterator();

}
