package interfaces;

/**
 * 13.03.2021 11:11
 * HomeWork10
 *
 * Интерфейс - расширяет список методов интерфейса InnoCollection
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface InnoList extends InnoCollection {
    /**
     * Метод - получение элемента по индексу
     *
     * @param index - индекс элемента
     * @return - элемент по индексу; -1 если вышли за границу
     */
    int get(int index);

    /**
     * Метод - заменяет элемент  в ячейке определенной индексом
     *
     * @param index   - индекс ячейки, в которую необходимо вставить элемент
     * @param element - элемент, который необходимо вставить; -1 если вышли за границу
     */
    void insert(int index, int element);

    /**
     * Метод - добавляет элемент в начало коллекции
     *
     * @param element - добавляемый элемент
     */
    void addToBegin(int element);

    /**
     * Метод - удаляет элемент в заданной позиции
     *
     * @param index - позиция элемента
     */
    void removeByIndex(int index);
}
