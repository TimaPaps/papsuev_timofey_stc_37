package interfaces;

/**
 * 13.03.2021 11:12
 * HomeWork10
 *
 * Итератор - курсор, который позволяет последовательно обходить какую-либо коллекцию
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface InnoIterator {
    /**
     * Метод - возвращает следующий элемент коллекции
     *
     * @return - элемент
     */
    int next();

    /**
     * Метод - проверяет, есть ли следующий элемент коллекции
     *
     * @return - true, если есть следующий элемент; false, если нет элемента
     */
    boolean hasNext();
}
