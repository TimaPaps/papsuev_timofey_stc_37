import classes.ShowProgram;

/**
 * 13.03.2021 11:08
 * HomeWork10
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        ShowProgram showArrayList = new ShowProgram();
        showArrayList.showInnoArrayList();

        ShowProgram showLinkedList = new ShowProgram();
        showLinkedList.showInnoLinkedList();
    }
}
