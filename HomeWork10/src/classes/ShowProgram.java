package classes;

import interfaces.InnoIterator;
import interfaces.InnoList;

/**
 * 14.03.2021 13:05
 * HomeWork10
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class ShowProgram {
    public void showInnoArrayList() {
        InnoList list = new InnoArrayList();
        InnoIterator iterator = list.iterator();
        InnoIterator iterator1 = list.iterator();
        InnoIterator iterator2 = list.iterator();
        InnoIterator iterator3 = list.iterator();
        InnoIterator iterator4 = list.iterator();
        InnoIterator iterator5 = list.iterator();
        InnoIterator iterator6 = list.iterator();

        System.out.println("Демо ArrayList");
        System.out.println("Добавление 11 элементов в массив на 10 элементов:");
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(-7);
        list.add(8);
        list.add(9);
        list.add(10);
        list.add(11);
        System.out.println("Длинна массива " + list.size());
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
        System.out.println();

        System.out.println("Показать элемент под индексом:");
        System.out.println(list.get(10));
        System.out.println(list.get(-1));
        System.out.println(list.get(55));
        System.out.println("Длинна массива " + list.size());
        while (iterator1.hasNext()) {
            System.out.print(iterator1.next() + " ");
        }
        System.out.println();
        System.out.println();

        System.out.println("Добавить элемент по индексу");
        list.insert(0, 44);
        list.insert(5, 44);
        list.insert(12, 44);
        list.insert(20, 44);
//        list.insert(-1, 44);
        System.out.println("Длинна массива " + list.size());
        while (iterator2.hasNext()) {
            System.out.print(iterator2.next() + " ");
        }
        System.out.println();
        System.out.println();

        System.out.println("Добавить элемент в начало массива");
        list.addToBegin(55);
        System.out.println("Длинна массива " + list.size());
        while (iterator3.hasNext()) {
            System.out.print(iterator3.next() + " ");
        }
        System.out.println();
        System.out.println();

        System.out.println("Удалить элемент по индексу из массива");
        list.removeByIndex(0);
        list.removeByIndex(8);

        list.removeByIndex(13);
//        list.removeByIndex(-1);
//        list.removeByIndex(55);
        System.out.println("Длинна массива " + list.size());
        while (iterator4.hasNext()) {
            System.out.print(iterator4.next() + " ");
        }
        System.out.println();
        System.out.println();

        System.out.println("Удалить элемент из массива");
        list.remove(44);
        System.out.println("Длинна массива " + list.size());
        while (iterator5.hasNext()) {
            System.out.print(iterator5.next() + " ");
        }
        System.out.println();
        System.out.println();

        System.out.println("Найти элемент в массиве");
        System.out.println(list.contains(-1));
        System.out.println(list.contains(15));
        System.out.println(list.contains(4));
        System.out.println("Длинна массива " + list.size());
        while (iterator6.hasNext()) {
            System.out.print(iterator6.next() + " ");
        }
        System.out.println();
        System.out.println();
    }

    public void showInnoLinkedList() {
        InnoList list = new InnoLinkedList();

        System.out.println("Демо LinkedList");
        System.out.println("Добавление элементов в список");
        list.add(1);
        list.add(2);
        list.add(3);
//        list.add(4);
//        list.add(5);
        System.out.println("Длинна списка " + list.size());
        InnoIterator iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
        System.out.println();

        System.out.println("Показать элемент под индексом:");
        System.out.println(list.get(2));
        System.out.println(list.get(-1));
        System.out.println(list.get(55));
        System.out.println("Длинна массива " + list.size());
        InnoIterator iterator1 = list.iterator();
        while (iterator1.hasNext()) {
            System.out.print(iterator1.next() + " ");
        }
        System.out.println();
        System.out.println();

        System.out.println("Добавить элемент в список по индексу");
        list.insert(2, 77);
        list.insert(0, 44);
        list.insert(5, 22);
        System.out.println("Длинна массива " + list.size());
        InnoIterator iterator2 = list.iterator();
        while (iterator2.hasNext()) {
            System.out.print(iterator2.next() + " ");
        }
        System.out.println();
        System.out.println();

        System.out.println("Добавить элемент в начало списка");
        list.addToBegin(55);
        System.out.println("Длинна массива " + list.size());
        InnoIterator iterator3 = list.iterator();
        while (iterator3.hasNext()) {
            System.out.print(iterator3.next() + " ");
        }
        System.out.println();
        System.out.println();

        System.out.println("Удалить из списка элемент по индексу");
//        list.removeByIndex(1);
        list.removeByIndex(2);
        System.out.println("Длинна массива " + list.size());
        InnoIterator iterator4 = list.iterator();
        while (iterator4.hasNext()) {
            System.out.print(iterator4.next() + " ");
        }
        System.out.println();
        System.out.println();

        System.out.println("Удалит элемент из списка");
        list.remove(77);
        InnoIterator iterator5 = list.iterator();
        System.out.println("Длинна массива " + list.size());
        while (iterator5.hasNext()) {
            System.out.print(iterator5.next() + " ");
        }
        System.out.println();
        System.out.println();

        System.out.println("Найти элемент в списке");
        System.out.println(list.contains(55));
        System.out.println(list.contains(77));
        System.out.println(list.contains(3));
        System.out.println(list.contains(-200));
        InnoIterator iterator6 = list.iterator();
        while (iterator6.hasNext()) {
            System.out.print(iterator6.next() + " ");
        }
        System.out.println();
    }
}
