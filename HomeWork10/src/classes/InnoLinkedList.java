package classes;

import interfaces.InnoIterator;
import interfaces.InnoList;

/**
 * 13.03.2021 11:13
 * HomeWork10
 * <p>
 * Класс - реализует список LinkedList
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class InnoLinkedList implements InnoList {
    /**
     * Класс - описывает объект Node
     */
    private static class Node {
        int value;
        Node next;

        Node(int value) {
            this.value = value;
        }

        /**
         * Метод - устанавливает ссылку на следующий элемент
         *
         * @param next - следующий элемент
         */
        public void setNext(Node next) {
            this.next = next;
        }
    }

    /**
     * Элемент - первый в списке
     */
    private Node first;
    /**
     * Элемент - последний в списке
     */
    private Node last;
    /**
     * Счетчик - количество элементов в списке
     */
    private int count;

    /**
     * @param index - индекс элемента
     * @return - элемент, если индекс в пределах списка и индекс не больше,
     * чем количество элементов в списке; -1 если вышли за границу
     */
    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            Node current = first;

            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            return current.value;
        } else {
            return -1;
        }
    }

    /**
     * @param index   - позиция в списке, по значению которой необходимо вставить элемент
     * @param element - элемент, который необходимо вставить в список
     */
    @Override
    public void insert(int index, int element) {
        // TODO: реализовать
        if (index >= 0 && index <= count) {
            Node newNode = new Node(element);
            Node current = first;

            if (index == 0) {
                first = newNode;
                first.next = current;
                count++;
                return;
            }

            for (int i = 1; i <= count; i++) {
                if (index == i) {
                    Node next = current.next;
                    current.setNext(newNode);
                    newNode.next = next;
                    count++;
                    break;
                }
                current = current.next;
            }
        } else if (index < 0) {
            System.err.println("Индекс не может быть отрицательным!");
        } else {
            System.err.println("Индекс за пределами списка. Индекс от 0 до " + count);
        }
    }

    /**
     * @param element - добавляемый элемент
     */
    @Override
    public void addToBegin(int element) {
        // TODO: реализовать
        Node newNode = new Node(element);
        Node current = first;

        if (first != null) {
            first = newNode;
            first.next = current;
            count++;
        } else {
            add(element);
        }
    }

    /**
     * @param index - позиция элемента, по значению которой необходимо удалить элемент
     */
    @Override
    public void removeByIndex(int index) {
        // TODO: реализовать
        if (index >= 0 && index < count) {
            Node current = first;

            if (index == 0) {
                first = current.next;
                count--;
                return;
            }

            for (int i = 1; i < count; i++) {
                if (index == i) {
                    Node next = current.next.next;
                    current.setNext(next);
                    count--;
                    break;
                }
                current = current.next;
            }
        } else if (index < 0) {
            System.err.println("Индекс не может быть отрицательным!");
        } else {
            System.err.println("Индекс за пределами списка. Индекс от 0 до " + (count - 1));
        }
    }

    /**
     * @param element - добавляемый элемент
     */
    @Override
    public void add(int element) {
        Node newNode = new Node(element);

        if (first == null) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        count++;
    }

    /**
     * @param element - удаляемый элемент
     */
    @Override
    public void remove(int element) {
        // TODO: реализовать
        Node current = first;
        Node previous = first;

        for (int i = 0; i < count; i++) {
            if (element == current.value && i == 0) {
                first = current.next;
                count--;
            } else if (element == current.value) {
                previous.setNext(current.next);
                count--;
                break;
            }
            previous = current;
            current = current.next;
        }
    }

    /**
     * @param element - искомый элемент
     * @return
     */
    @Override
    public boolean contains(int element) {
        // TODO: реализовать
        Node current = first;

        for (int i = 0; i < count; i++) {
            if (element == current.value) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    /**
     * @return - количество элементов в массиве
     */
    @Override
    public int size() {
        return count;
    }

    /**
     * @return - новый объект итератора
     */
    @Override
    public InnoIterator iterator() {
        return new InnoLinkedListIterator();
    }

    /**
     * Внутренний класс - реализует интерфейс InnoIterator
     */
    private class InnoLinkedListIterator implements InnoIterator {
        /**
         * Текущая позиция
         */
        private Node next;

        InnoLinkedListIterator() {
            this.next = first;
        }

        /**
         * @return - возвращает следующий элемент коллекции
         */
        @Override
        public int next() {
            int nextValue = next.value;
            next = next.next;
            return nextValue;
        }

        /**
         * @return - true, если есть следующий элемент в массиве; false, если элемента нет
         */
        @Override
        public boolean hasNext() {
            return next != null;
        }
    }
}
