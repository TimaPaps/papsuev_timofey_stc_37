package classes;

import interfaces.InnoIterator;
import interfaces.InnoList;

/**
 * 13.03.2021 11:12
 * HomeWork10
 * <p>
 * Класс - реализует список ArrayList
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class InnoArrayList implements InnoList {
    private static final int DEFAULT_SIZE = 10;

    /**
     * Массив - хранит элементы списка
     */
    private int[] elements;

    /**
     * Счетчик - количество элементов в массиве
     */
    private int count;

    public InnoArrayList() {
        this.elements = new int[DEFAULT_SIZE];
        this.count = 0;
    }

    /**
     * @param index - индекс элемента
     * @return - элемент, если индекс в пределах массива и индекс не больше,
     * чем количество элементов в массиве; -1 если вышли за границу
     */
    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            return -1;
        }
    }

    /**
     * Метод - создает массив, размер массива равен размеру исходного массива
     *
     * @return - пустой массив
     */
    private int[] copyArray() {
        int[] tempArray = new int[elements.length];
        return tempArray;
    }

    /**
     * @param index   - индекс ячейки, в которую необходимо вставить элемент
     * @param element - элемент, который необходимо вставить в массив
     */
    @Override
    public void insert(int index, int element) {
        // TODO: реализовать
        if (count == elements.length) {
            resize();
        }
        int[] tempArray = copyArray();

        if (index >= 0 && index <= count) {
            for (int i = 0; i < index; i++) {
                tempArray[i] = elements[i];
            }
            tempArray[index] = element;

            for (int i = index; i < count; i++) {
                tempArray[i + 1] = elements[i];
            }
            elements = tempArray;
            count++;
        } else if (index > count) {
            index = count;

            for (int i = 0; i < count; i++) {
                tempArray[i] = elements[i];
            }
            tempArray[index] = element;
            elements = tempArray;
            count++;
        } else {
            System.err.println("Индекс не может быть отрицательным!");
        }
    }

    /**
     * @param element - добавляемый элемент
     */
    @Override
    public void addToBegin(int element) {
        // TODO: реализовать
        if (count == elements.length) {
            resize();
        }
        int[] tempArray = copyArray();
        tempArray[0] = element;

        for (int i = 0; i < tempArray.length - 1; i++) {
            tempArray[i + 1] = elements[i];
        }
        elements = tempArray;
        count++;
    }

    /**
     * @param index - позиция элемента
     */
    @Override
    public void removeByIndex(int index) {
        // TODO: реализовать
        if (index >= 0 && index < count) {

            for (int i = 0; i < index; i++) {
                elements[i] = elements[i];
            }
            for (int i = index; i < count; i++) {
                elements[i] = elements[i + 1];
            }
            count--;
        } else if (index >= count) {
            System.err.println("Индекс больше, чем количество чисел в массиве!");
        } else if (index < 0) {
            System.err.println("Индекс не может быть отрицательным!");
        }
    }

    /**
     * @param element - добавляемый элемент
     */
    @Override
    public void add(int element) {
        if (count == elements.length) {
            resize();
        }
        elements[count++] = element;
    }

    /**
     * Метод - увеличивает массив на половину исходного, методом перезаписи
     * исходного массива
     */
    private void resize() {
        int newElements[] = new int[elements.length + elements.length / 2];

        for (int i = 0; i < count; i++) {
            newElements[i] = elements[i];
        }
        this.elements = newElements;
    }

    /**
     * @param element - удаляемый элемент
     */
    @Override
    public void remove(int element) {
        // TODO: реализовать
        int remoteCount = 0;

        for (int i = 0; i < elements.length; i++) {

            if (element == elements[i]) {
                remoteCount++;
                count--;
            } else {
                elements[i - remoteCount] = elements[i];
            }
        }
    }

    /**
     * @param element - искомый элемент
     * @return - true, если элемент найден в массиве; false, если не найден
     */
    @Override
    public boolean contains(int element) {
        // TODO: реализовать
        for (int i = 0; i < elements.length; i++) {
            if (element == elements[i]) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return - количество элементов в массиве
     */
    @Override
    public int size() {
        return count;
    }

    /**
     * @return - новый объект итератора
     */
    @Override
    public InnoIterator iterator() {
        return new InnoArrayListIterator();
    }

    /**
     * Внутренний класс - реализует интерфейс InnoIterator
     */
    private class InnoArrayListIterator implements InnoIterator {
        /**
         * Текущая позиция
         */
        private int currentPosition;

        /**
         * @return - возвращает следующий элемент коллекции
         */
        @Override
        public int next() {
            int nextValue = elements[currentPosition];
            currentPosition++;
            return nextValue;
        }

        /**
         * @return - true, если есть следующий элемент в массиве; false, если элемента нет
         */
        @Override
        public boolean hasNext() {
            return currentPosition < count;
        }
    }
}
