import java.util.Scanner;
import java.util.Arrays;									

/**
 *Приложение, со следующим набором функций и процедур:
 *1) выводит сумму элементов массива.
 *2) выполняет разворот массива (массив вводится с клавиатуры).
 *3) вычисляет среднее арифметическое элементов массива (массив 
 *	 вводится с клавиатуры).
 *4) меняет местами максимальный и минимальный элементы массива.
 *5) выполняет сортировку массива методом пузырька.
 *6) выполняет преобразование массива в число.
 *
 *@autor Timofey Papsuev
 *@version 2.0
 */

class Program {
	public static void main(String[] args) {
		selectProcedure();
	}

	/**
	 *Процедура запуска одной из 6 поцедур:
	 *1) sumElementsArray;
	 *2) mirrorArray;
	 *3) averageArithmetical;
	 *4) reversMinMaxElementsArray;
	 *5) arrayBubleSort;
	 *6) arrayToNumber;
	 */
	public static void selectProcedure() {
		Scanner scanner = new Scanner(System.in);
		int number;

		while (true) {
			System.out.print("Enter number from 1 to 6");
			System.out.println(" to select a procedure");
			number = scanner.nextInt();	

			if (number < 1 || number > 6) {
				System.out.println("Entered number " + number);
				continue;
			} else {
				break;
			}
		}

		if (number == 1) {
			System.out.println("Procedure: sumElementsArray");
			sumElementsArray();
		}

		if (number == 2) {
			System.out.println("Procedure: mirrorArray");
			mirrorArray();
		}

		if (number == 3) {
			System.out.println("Procedure: averageArithmetical");
			averageArithmetical();
		}

		if (number == 4) {
			System.out.println("Procedure: reversMinMaxElementsArray");
			reversMinMaxElementsArray();
		}

		if (number == 5) {
			System.out.println("Procedure: arrayBubleSort");
			arrayBubleSort();
		}

		if (number == 6) {
			System.out.println("Procedure: arrayToNumber");
			arrayToNumber();
		}
	}

	/**
	 *Процедура выводит сумму элементов массива
	 */
	public static void sumElementsArray() {
		int[] array = {4, -7, 3, 9, 17, 7, 5};
		int sumElementsArray = 0;
		System.out.println("Array: " + Arrays.toString(array));

		for (int i = 0; i < array.length; i++) {
			sumElementsArray += array[i];
		}

		System.out.println("Answer: " + sumElementsArray);
	}

	/**
	 *Функция - создание массива целых чисел ввенных с клавиатуры
	 *@return возвращает массив целых чисел
	 */
	public static int[] arrayEnteredIntegerNumbers() {
		Scanner scanner = new Scanner(System.in);
		int number;

		while (true) {
			System.out.println("Enter integer number > 0");
			number = scanner.nextInt();

			if (number < 1) {
				System.out.println("Entered number " + number);
				continue;
			} else {
				break;
			}
		}

		System.out.println("Fill the array");
		int[] array = new int[number];

		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}

		return array;
	}

	/**
	 *Процедура выполняет разворот массива (массив вводится с 
	 *клавиатуры)
	 */
	public static void mirrorArray() {
		int[] array = arrayEnteredIntegerNumbers();
		System.out.println("Array before: " + Arrays.toString(array));
		int[] mirrorArray = new int[array.length];
		int count = mirrorArray.length - 1;

		for (int i = 0; i < mirrorArray.length; i++) {
			mirrorArray[count] = array[i];
			count--;
		}

		System.out.println("Array after: " + Arrays.toString(mirrorArray));
	}

	/**
	 *Процедура вычисляет среднее арифметическое элементов массива 
	 *(массив вводится с клавиатуры)
	 */
	public static void averageArithmetical() {
		int[] array = arrayEnteredIntegerNumbers();
		System.out.println("Array before: " + Arrays.toString(array));
		double averageArithmetical = 0;

		for (int i = 0; i < array.length; i++) {
			averageArithmetical += array[i];
		}

		averageArithmetical /= array.length;
		System.out.println("Answer " + averageArithmetical);
	}

	/**
	 *Процедура меняет местами максимальный и минимальный элементы 
	 *массива
	 */
	public static void reversMinMaxElementsArray() {
		int[] array = {8, -53, 46, -3, 27, 77, -41};
		System.out.println("Array before: " + Arrays.toString(array));
		int min = 0;
		int max = 0;
		int minPosition = -1;
		int maxPosition = -1;

		for (int i = 0; i < array.length; i++) {
			if (i == 0) {
				min = array[i];
				max = array[i];
			}

			if (array[i] <= min) {
				min = array[i];
				minPosition = i;
			}

			if (array[i] >= max) {
				max = array[i];
				maxPosition = i;
			}
		}

		if (minPosition >= 0 && maxPosition >= 0) {
			array[minPosition] = max;
			array[maxPosition] = min;
		}

		System.out.println("Array after: " + Arrays.toString(array));
	}

	/**
	 *Процедура выполняет сортировку массива методом пузырька
	 */
	public static void arrayBubleSort() {
		int[] array = {8, -53, 46, -3, 27, 77, -41};
		System.out.println("Array before: " + Arrays.toString(array));
		boolean isSorted = false;
		int temp;

		while (isSorted == false) {
			isSorted = true;

			for (int i = 0; i < array.length - 1; i++) {
				if (array[i] > array[i + 1]) {
					isSorted = false;

					temp = array[i];
					array[i] = array[i + 1];
					array[i + 1] = temp;
				}
			}
		}

		System.out.println("Array after: " + Arrays.toString(array));
	}

	/**
	 *Процедура выполняет преобразование массива в число
	 */
	public static void arrayToNumber() {
		int[] array = {4, 2, -3303, 0, 77};
		int number = 0;
		int power = 0;
		System.out.println("Array: " + Arrays.toString(array));

		for (int i = array.length - 1; i >= 0; i--) {

			if (array[i] < 0) {
				array[i] *= (-1);
			}		

			number += array[i] * Math.pow(10, power);

			power += powerOfNumber(array[i]);
		}

		System.out.println("Number: " + number);
	}

	/**
	 *Функция определения порядка числа
	 *@return целое число - порядок полученного числа
	 */
	public static int powerOfNumber(int number) {
		int count = 0;

		if (number < 0) {
			number *= (-1);
		}

		if (number == 0) {
			count = 1;
		} else {
			while (number > 0) {
				number /= 10;
				count++;
			}			
		}

		return count;
	}
}