import java.util.Scanner;

/** 
 *Приложение, рассчитывающее определенный интеграл методом Симпсона по
 *формуле из задания.
 *
 *@autor Timofey Papsuev
 *@version 1.0
 */

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		double fromA = scanner.nextDouble();
		double toB = scanner.nextDouble();
		int[] steps = {10, 100, 1_000, 10_000, 100_000, 1_000_000};
		double[] area = new double[steps.length];

		calcIntegralOnSteps(fromA, toB, steps, area);
		printCalcIntegralOnSteps(steps, area);
	}

	/**
	 *Функция возведения числа в квадрат.
	 *@return возвращает квадрат числа.
	 */
	public static double square(Double x) {
		return x * x;
	}

	/**
	 *Функция расчитывает определенный интеграл Методом симпсона,
	 *@return возвращает интеграл.
	 */
	public static double calcIntegral(double fromA, double toB, int step) {
		double h = (toB - fromA) / step;
		double integral = 0;

		for (double x = fromA + h; x <= (toB - h); x += 2 * h) {
			double currentRectangle = square(x - h) + 4 * square(x) + square(x + h);

			if (currentRectangle < 0) {
				currentRectangle *= (-1);
			}

			integral += currentRectangle;
		}

		integral = h / 3 * integral;

		return integral;
	}

	/**
	 *Процедура для разбиений по шагам при расчете определенного 
	 *интеграла.
	 */
	public static void calcIntegralOnSteps(double fromA, 
										   double toB, 
										   int[] steps, 
										   double[] area) {

		for (int i = 0; i < steps.length; i++) {
			area[i] = calcIntegral(fromA, toB, steps[i]);
		}
	}

	/**
	 *Процедура форматирования вывода расчитанного определенного
	 *интеграла.
	 */
	public static void printCalcIntegralOnSteps(int[] steps, double[] area) {
		System.out.printf("%7s| %9s| \n", "steps", "area");
		System.out.println("-------------------------------");

		for (int i = 0; i < steps.length; i++) {
			System.out.printf("%7d| %5.5f| \n", steps[i], area[i]);
		}
	}
}