/**
 * 25.02.2021 23:29
 * HomeWork7
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Main {

    public static void main(String[] args) {
        User user = User.builder()
                .firstName("Tima")
                //.lastName("Papsuev")
                .age(25)
                //.isWorked(true)
                .build();

        //код ниже для наглядности вывода
        if (user.getFirstName() != null) {
            System.out.println(user.getFirstName());
        } else {
            System.out.println("Пользователь решил не сообщать свое имя");
        }

        if (user.getLastName() != null) {
            System.out.println(user.getLastName());
        } else {
            System.out.println("Пользователь решил не сообщать свою фамилию");
        }

        if (user.getAge() != 0) {
            System.out.println(user.getAge());
        } else {
            System.out.println("Пользователь решил не сообщать свой возраст");
        }

        if (user.isWorked()) {
            System.out.println("Пользователь работает");
        } else {
            System.out.println("Пользователь не указал работает ли он," +
                    " по умолчанию - не работает");
        }
    }
}
