/**
 * Класс - Пользователь. Создание экземпляра класса осуществляется с
 * применением паттерна Builder.
 *
 * 25.02.2021 23:29
 * HomeWork7
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class User {
    /** Поле Имя */
    private final String firstName;
    /** Поле Фамилия */
    private final String lastName;
    /** Поле Возраст*/
    private final int age;
    /** Поле Работает */
    private final boolean isWorked;

    /**
     * Конструктор класса User
     * @param builder - ссылка на объект Builder
     */
    private User(Builder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.age = builder.age;
        this.isWorked = builder.isWorked;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public boolean isWorked() {
        return isWorked;
    }

    /**
     * Метод для создания объекта Builder
     * @return возвращает объект Builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Класс Builder создает объект с полями, значения которых
     * получены при создании экземпляра класса User
     * */
    public static class Builder {
        /** Поле Имя, значение по умолчанию null*/
        private String firstName;
        /** Поле Фамилия, значение по умолчанию null*/
        private String lastName;
        /** Поле Возраст, значение по умолчанию 0*/
        private int age = 0;
        /** Поле Работает, значение по умолчанию false*/
        private  boolean isWorked = false;

        /**
         * @param value - имя пользователя
         * @return возвращает указатель на себя (объект Builder)
         */
        public Builder firstName(String value) {
            firstName = value;
            return this;
        }

        /**
         * @param value - фамилия пользователя
         * @return возвращает указатель на себя (объект Builder)
         */
        public Builder lastName(String value) {
            lastName = value;
            return this;
        }

        /**
         * @param value - возраст пользователя
         * @return возвращает указатель на себя (объект Builder)
         */
        public Builder age(int value) {
            age = value;
            return this;
        }

        /**
         * @param value - true если работает, false если не работает
         * @return возвращает указатель на себя (объект Builder)
         */
        public Builder isWorked(boolean value) {
            isWorked = value;
            return this;
        }

        /**
         * Метод генерации объекта
         * @return возвращает указатель на созданный объект User
         */
        public User build() {
            return new User(this);
        }
    }
}
