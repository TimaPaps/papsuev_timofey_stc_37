/**
 *Реализация приложения, которое выводит двоичное представление 
 *пятизначного числа (значение числа задается непосредственно в коде).
 *В данном приложении запрещено использовать циклы и массивы.
 *
 *my second code documentation 8)
 *@autor Timofey Papsuev
 *@version 2.0
 */
class Program {
	public static void main(String[] args) {
		int number = 12345;
		String binaryNumber = "";

		if (number != 0) {
			binaryNumber = number % 2 + binaryNumber;
			number /= 2;
		}
		if (number != 0) {
			binaryNumber = number % 2 + binaryNumber;
			number /= 2;
		}
		if (number != 0) {
			binaryNumber = number % 2 + binaryNumber;
			number /= 2;
		}
		if (number != 0) {
			binaryNumber = number % 2 + binaryNumber;
			number /= 2;
		}
		if (number != 0) {
			binaryNumber = number % 2 + binaryNumber;
			number /= 2;
		}
		if (number != 0) {
			binaryNumber = number % 2 + binaryNumber;
			number /= 2;
		}
		if (number != 0) {
			binaryNumber = number % 2 + binaryNumber;
			number /= 2;
		}
		if (number != 0) {
			binaryNumber = number % 2 + binaryNumber;
			number /= 2;
		}
		if (number != 0) {
			binaryNumber = number % 2 + binaryNumber;
			number /= 2;
		}
		if (number != 0) {
			binaryNumber = number % 2 + binaryNumber;
			number /= 2;
		}
		if (number != 0) {
			binaryNumber = number % 2 + binaryNumber;
			number /= 2;
		}
		if (number != 0) {
			binaryNumber = number % 2 + binaryNumber;
			number /= 2;
		}
		if (number != 0) {
			binaryNumber = number % 2 + binaryNumber;
			number /= 2;
		}
		if (number != 0) {
			binaryNumber = number % 2 + binaryNumber;
			number /= 2;
		}

		System.out.println(binaryNumber);

		//Простой вариант для решения задачи
		//int number = 12345;
		//System.out.println(Integer.toBinaryString(number));
    }
}