import java.util.Scanner;
import java.util.ArrayList;

/**
 *Реализация приложения, которое для заданной последовательности чисел 
 *считает произведение тех чисел, сумма цифр которых - простое число.
 *Последнее число последовательности - 0.
 *
 *my third code documentation 8)
 *@autor Timofey Papsuev
 *@version 2.0
 */
class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int currentNumber;

		while (true) {
			System.out.println("Enter number > 1");
			currentNumber = scanner.nextInt();

			if (currentNumber <= 1) {
				System.out.println("Entered number " + currentNumber);		
				continue;
			} else {
				break;
			}
		}
		System.out.println("Enter numbers > 1 or 0 to exit");

		ArrayList<Integer> simpleNumbers = new ArrayList<>();

		/*
		разложение вводимых чисел на цифры и провверка суммы
		цифр на простое/составное число с последующей записью в
		списочный массив простых чисел
		*/
		while (currentNumber != 0) {
			if (currentNumber < 0 || currentNumber == 1) {
				System.out.println("Entered number " + currentNumber);
				System.out.println("Enter number > 1");
				currentNumber = scanner.nextInt();
				continue;
			} else {
				int summ = 0;
				int temp = currentNumber;
				boolean isPrime = true;
				
				while (temp != 0) {
					summ = summ + temp % 10;
					temp = temp / 10;
				}

				/*
				мой вариант
				*/
				if (summ != 1) {
					for (int i = 2; i < summ; i++) {
						if (summ % i == 0) {
							isPrime = false;
							break;
						} 
					}
					if (isPrime == true) {
						simpleNumbers.add(currentNumber);
					}
				}

				/*
				вариант с лекции с моей доработкой под задачу
				*/
//				if (summ == 2 || summ == 3) {
//					simpleNumbers.add(currentNumber);
//				}
//				for (int i = 2; i * i <= summ; i++) {
//					if (summ % i == 0) {
//						isPrime = false;
//						break;
//					} else {
//						simpleNumbers.add(currentNumber);
//					}
//				}

				summ = 0;
				currentNumber = scanner.nextInt();
			} 								
		}

		long result = 1;
		String answer = "";

		/*
		формирование ответа для вывода
		*/
		if (simpleNumbers.size() == 0) {
			System.out.println("Answer: no primes entered");
		} else if (simpleNumbers.size() == 1) {
			System.out.println("Answer: " + simpleNumbers.get(0));
		} else {
			for (int i = 0; i < simpleNumbers.size(); i++) {
				result = result * simpleNumbers.get(i);
				if (i == 0) {
					answer = answer + simpleNumbers.get(i);
		
				} else {
					answer = answer + " * " + simpleNumbers.get(i);
				}
			}
			System.out.println("Answer: " + answer + " = " + result);			
		}
	}
}