/**
 *Реализация приложения, которое выводит сумму цифр пятизначного числа 
 *(значение числа задается непосредственно в коде).
 *В данном приложении запрещено использовать циклы и массивы.
 *
 *my first code documentation 8)
 *@autor Timofey Papsuev
 *@version 1.0
 *
 *Решение применимо только для пятизначного числа
 */
 class Program {
 	public static void main(String[] args) {
 		int number = 44449;

		int result = number % 10;
		int reduced = number / 10;

		result = result + reduced % 10;
		reduced = reduced / 10;		
		result = result + reduced % 10;
		reduced = reduced / 10;	
		result = result + reduced % 10;
		reduced = reduced / 10;	
		result = result + reduced;
	
		System.out.println(result);
 	}
 }