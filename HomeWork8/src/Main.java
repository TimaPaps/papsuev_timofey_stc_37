import ru.innojava.geometricFigures.base.ShowFigure;

/**
 * 01.03.2021 10:35
 * HomeWork8
 *
 * @author Papsuev Timofey
 * @version v1.0
 */

public class Main {

    public static void main(String[] args) {
        ShowFigure.showCircle(4,3,7,0,-4,2);
        ShowFigure.showEllipse(-7,-4.5,7,2,1,1.9,2.5);
        ShowFigure.showRectangle(0,5,5,7,5,4,1.3);
        ShowFigure.showSquare(0,0,4, 5,1,3);
    }
}
