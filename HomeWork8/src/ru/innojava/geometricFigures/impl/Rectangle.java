package ru.innojava.geometricFigures.impl;

import ru.innojava.geometricFigures.base.AbstractGeometricFigures;

/**
 * 01.03.2021 10:58
 * HomeWork8
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Rectangle extends AbstractGeometricFigures {
    /**
     * Поле - константа - Название
     */
    public static final String RECTANGLE_NAME = "Прямоугольник";

    /**
     * Поле - Сторона А
     */
    private double sideA;
    /**
     * Поле - Сторона В
     */
    private double sideB;

    /**
     * @param coordinateX - значение ординаты
     * @param coordinateY - значение абсциссы
     * @param sideA       - значение длины стороны А
     * @param sideB       - значение длины стороны В
     */
    public Rectangle(double coordinateX, double coordinateY, double sideA, double sideB) {
        super(coordinateX, coordinateY);
        this.sideA = sideA;
        this.sideB = sideB;
    }

    public double getSideA() {
        return sideA;
    }

    public double getSideB() {
        return sideB;
    }

    /**
     * Метод - выводит имя фигуры
     */
    @Override
    public void printInfo() {
        System.out.println(RECTANGLE_NAME);
    }

    /**
     * Метод - расчитывает площадь прямоугольника
     *
     * @return возвращает площадь прямоугольника
     */
    @Override
    public double square() {
        square = sideA * sideB;
        return square;
    }

    /**
     * Метод - расчитывает периметр прямоугольника
     *
     * @return возвращает периметр прямоугольника
     */
    @Override
    public double perimeter() {
        perimeter = 2 * (sideA + sideB);
        return perimeter;
    }

    /**
     * Метод - изменяет масштаб прямоугольника
     *
     * @param coefficient - коэффициент масштаба
     */
    @Override
    public void scaleFigure(double coefficient) {
        this.sideA *= coefficient;
        this.sideB *= coefficient;
    }
}
