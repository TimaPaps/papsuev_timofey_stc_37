package ru.innojava.geometricFigures.impl;

import ru.innojava.geometricFigures.base.AbstractGeometricFigures;

/**
 * 01.03.2021 10:58
 * HomeWork8
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Circle extends AbstractGeometricFigures {
    /**
     * Поле - константа - Название
     */
    public static final String CIRCLE_NAME = "Круг";

    /**
     * Поле - Радиус
     */
    private double radius;

    /**
     * @param coordinateX - значение ординаты
     * @param coordinateY - значение абсциссы
     * @param radius      - радиус круга
     */
    public Circle(double coordinateX, double coordinateY, double radius) {
        super(coordinateX, coordinateY);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    /**
     * Метод - выводит имя фигуры
     */
    @Override
    public void printInfo() {
        System.out.println(CIRCLE_NAME);
    }

    /**
     * Метод - расчитывает площадь круга
     *
     * @return возвращает площадь круга
     */
    @Override
    public double square() {
        square = Math.PI * Math.pow(radius, 2);
        return square;
    }

    /**
     * Метод - расчитывает периметр круга
     *
     * @return возвращает периметр круга
     */
    @Override
    public double perimeter() {
        perimeter = 2 * Math.PI * radius;
        return perimeter;
    }

    /**
     * Метод - изменяет масштаб круга
     *
     * @param coefficient - коэффициент масштаба
     */
    @Override
    public void scaleFigure(double coefficient) {
        this.radius *= coefficient;
    }
}
