package ru.innojava.geometricFigures.impl;

import ru.innojava.geometricFigures.base.AbstractGeometricFigures;

/**
 * 01.03.2021 10:57
 * HomeWork8
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Ellipse extends AbstractGeometricFigures {
    /**
     * Поле - константа - Название
     */
    public static final String ELLIPSE_NAME = "Эллипс";

    /**
     * Поле - Большая полуось
     */
    private double bigSemiAxis;
    /**
     * Поле - Маленькая полуось
     */
    private double smallSemiAxis;

    /**
     * @param coordinateX   - значение ординаты
     * @param coordinateY   - значение абсциссы
     * @param bigSemiAxis   - значение длины большой полуоси
     * @param smallSemiAxis - значение длины маленькой полуоси
     */
    public Ellipse(double coordinateX, double coordinateY, double bigSemiAxis, double smallSemiAxis) {
        super(coordinateX, coordinateY);
        this.bigSemiAxis = bigSemiAxis;
        this.smallSemiAxis = smallSemiAxis;
    }

    public double getBigSemiAxis() {
        return bigSemiAxis;
    }

    public double getSmallSemiAxis() {
        return smallSemiAxis;
    }

    /**
     * Метод - выводит имя фигуры
     */
    @Override
    public void printInfo() {
        System.out.println(ELLIPSE_NAME);
    }

    /**
     * Метод - расчитывает площадь эллипса
     *
     * @return возвращает площадь эллипса
     */
    @Override
    public double square() {
        square = Math.PI * bigSemiAxis * smallSemiAxis;
        return square;
    }

    /**
     * Метод - расчитывает периметр эллипса
     *
     * @return возвращает периметр эллипса
     */
    @Override
    public double perimeter() {
        perimeter = 2 * Math.PI * Math.sqrt((Math.pow(bigSemiAxis, 2) + Math.pow(smallSemiAxis, 2)) / 2);
        return perimeter;
    }

    /**
     * Метод - изменяет масштаб эллипса
     *
     * @param coefficient - коэффициент масштаба
     */
    @Override
    public void scaleFigure(double coefficient) {
        this.bigSemiAxis *= coefficient;
        this.smallSemiAxis *= coefficient;
    }
}
