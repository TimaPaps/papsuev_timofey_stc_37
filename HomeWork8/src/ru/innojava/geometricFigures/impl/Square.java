package ru.innojava.geometricFigures.impl;

/**
 * 01.03.2021 10:59
 * HomeWork8
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Square extends Rectangle {
    /**
     * Поле - константа - Название
     */
    public static final String SQUARE_NAME = "Квадрат";

    /**
     * @param coordinateX - значение ординаты
     * @param coordinateY - значение абсциссы
     * @param side        - значение длины стороны
     */
    public Square(double coordinateX, double coordinateY, double side) {
        super(coordinateX, coordinateY, side, side);
    }

    /**
     * Метод - выводит имя фигуры
     */
    @Override
    public void printInfo() {
        System.out.println(SQUARE_NAME);
    }
}
