package ru.innojava.geometricFigures.base;

/**
 * 01.03.2021 11:00
 * HomeWork8
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface Scalable {
    /**
     * Метод - изменяет масштаб фигуры
     *
     * @param coefficient - коэффициент масштаба
     */
    void scaleFigure(double coefficient);
}
