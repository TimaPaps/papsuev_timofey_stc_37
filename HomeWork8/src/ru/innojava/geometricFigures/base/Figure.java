package ru.innojava.geometricFigures.base;

/**
 * 01.03.2021 12:54
 * HomeWork8
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface Figure {
    /**
     * Метод - выводит текст
     */
    void printInfo();

    /**
     * Метод - расчитывает площадь фигуры
     *
     * @return возвращает площадь фигуры
     */
    double square();

    /**
     * Метод - расчитывает периметр фигуры
     *
     * @return возвращает периметр фигуры
     */
    double perimeter();
}
