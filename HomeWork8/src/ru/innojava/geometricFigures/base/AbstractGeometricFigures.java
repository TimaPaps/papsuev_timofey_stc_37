package ru.innojava.geometricFigures.base;

/**
 * 01.03.2021 10:55
 * HomeWork8
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public abstract class AbstractGeometricFigures implements Figure, Scalable, Relocatable {
    /**
     * Поле - Площадь
     */
    protected double square = 0;
    /**
     * Поле - Периметр
     */
    protected double perimeter = 0;
    /**
     * Поле - Координата X
     */
    protected double coordinateX;
    /**
     * Поле - Координата Y
     */
    protected double coordinateY;

    /**
     * @param coordinateX - значение ординаты
     * @param coordinateY - значение абсциссы
     */
    public AbstractGeometricFigures(double coordinateX, double coordinateY) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
    }

    public double getCoordinateX() {
        return coordinateX;
    }

    public double getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateX(double coordinateX) {
        this.coordinateX = coordinateX;
    }

    public void setCoordinateY(double coordinateY) {
        this.coordinateY = coordinateY;
    }

    /**
     * Метод - изменяет координаты центра фигуры
     *
     * @param newCoordinateX - новое значение ординаты
     * @param newCoordinateY - новое значение абсциссы
     */
    @Override
    public void moveCenter(double newCoordinateX, double newCoordinateY) {
        this.setCoordinateX(newCoordinateX);
        this.setCoordinateY(newCoordinateY);
    }
}
