package ru.innojava.geometricFigures.base;

/**
 * 01.03.2021 11:01
 * HomeWork8
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface Relocatable {
    /**
     * Метод - изменяет координаты центра фигуры
     *
     * @param newValueX - новое значение ординаты
     * @param newValueY - новое значение абсциссы
     */
    void moveCenter(double newValueX, double newValueY);
}
