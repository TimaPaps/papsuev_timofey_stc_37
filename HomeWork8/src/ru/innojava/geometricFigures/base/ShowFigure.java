package ru.innojava.geometricFigures.base;

import ru.innojava.geometricFigures.impl.Circle;
import ru.innojava.geometricFigures.impl.Ellipse;
import ru.innojava.geometricFigures.impl.Rectangle;
import ru.innojava.geometricFigures.impl.Square;

/**
 * 04.03.2021 15:08
 * HomeWork8
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class ShowFigure {
    /**
     * Поле - константа - SQUARE
     */
    public static final String SQUARE = "Площадь: ";
    /**
     * Поле - константа - NEW_SQUARE
     */
    public static final String NEW_SQUARE = "Новая площадь: ";
    /**
     * Поле - константа SIZE
     */
    public static final String SIZE = "Исходный размер: ";
    /**
     * Поле - константа NEW_SIZE
     */
    public static final String NEW_SIZE = "Новый размер: ";
    /**
     * Поле - константа - PERIMETER
     */
    public static final String PERIMETER = "Периметр: ";
    /**
     * Поле - константа - NEW_PERIMETER
     */
    public static final String NEW_PERIMETER = "Новый периметр: ";
    /**
     * Поле - кностанта - COORDINATES
     */
    public static final String COORDINATES = "Исходные координаты: ";
    /**
     * Поле - кностанта - NEW_COORDINATES
     */
    public static final String NEW_COORDINATES = "Новые координаты: ";

    public static void showCircle(double coordinateX,
                                  double coordinateY,
                                  double radius,
                                  double newCoordinateX,
                                  double newCoordinateY,
                                  double coefficient) {
        Circle circle = new Circle(coordinateX, coordinateY, radius);
        circle.printInfo();
        System.out.println(COORDINATES + "X = " + circle.getCoordinateX() + "; Y = " + circle.getCoordinateY());
        System.out.println(SIZE + "Радиус = " + circle.getRadius());
        System.out.println(SQUARE + circle.square());
        System.out.println(PERIMETER + circle.perimeter());
        circle.moveCenter(newCoordinateX, newCoordinateY);
        System.out.println(NEW_COORDINATES + "X = " + circle.getCoordinateX() + "; Y = " + circle.getCoordinateY());
        circle.scaleFigure(coefficient);
        System.out.println(NEW_SIZE + "Радиус = " + circle.getRadius());
        System.out.println(NEW_SQUARE + circle.square());
        System.out.println(NEW_PERIMETER + circle.perimeter());
        System.out.println();
    }

    public static void showEllipse(double coordinateX,
                                   double coordinateY,
                                   double bigSemiAxis,
                                   double smallSemiAxis,
                                   double newCoordinateX,
                                   double newCoordinateY,
                                   double coefficient) {
        Ellipse ellipse = new Ellipse(coordinateX, coordinateY, bigSemiAxis, smallSemiAxis);
        ellipse.printInfo();
        System.out.println(COORDINATES + "X = " + ellipse.getCoordinateX() + "; Y = " + ellipse.getCoordinateY());
        System.out.println(SIZE + "Большая полуось = " + ellipse.getBigSemiAxis() + "; Маленькая полуось = "  + ellipse.getSmallSemiAxis());
        System.out.println(SQUARE + ellipse.square());
        System.out.println(PERIMETER + ellipse.perimeter());
        ellipse.moveCenter(newCoordinateX, newCoordinateY);
        System.out.println(NEW_COORDINATES + "X = " + ellipse.getCoordinateX() + "; Y = " + ellipse.getCoordinateY());
        ellipse.scaleFigure(coefficient);
        System.out.println(NEW_SIZE + "Большая полуось = " + ellipse.getBigSemiAxis() + "; Маленькая полуось = " + ellipse.getSmallSemiAxis());
        System.out.println(NEW_SQUARE + ellipse.square());
        System.out.println(NEW_PERIMETER + ellipse.perimeter());
        System.out.println();
    }

    public static void showRectangle(double coordinateX,
                                     double coordinateY,
                                     double sideA,
                                     double sideB,
                                     double newCoordinateX,
                                     double newCoordinateY,
                                     double coefficient) {
        Rectangle rectangle = new Rectangle(coordinateX, coordinateY, sideA, sideB);
        rectangle.printInfo();
        System.out.println(COORDINATES + "X = " + rectangle.getCoordinateX() + "; Y = " + rectangle.getCoordinateY());
        System.out.println(SIZE + "Сторона А = " + rectangle.getSideA() + "; Сторона Б = " + rectangle.getSideB());
        System.out.println(SQUARE + rectangle.square());
        System.out.println(PERIMETER + rectangle.perimeter());
        rectangle.moveCenter(newCoordinateX, newCoordinateY);
        System.out.println(NEW_COORDINATES + "X = " + rectangle.getCoordinateX() + "; Y = " + rectangle.getCoordinateY());
        rectangle.scaleFigure(coefficient);
        System.out.println(NEW_SIZE + "Сторона А = " + rectangle.getSideA() + "; Сторона Б = " + rectangle.getSideB());
        System.out.println(NEW_SQUARE + rectangle.square());
        System.out.println(NEW_PERIMETER + rectangle.perimeter());
        System.out.println();
    }

    public static void showSquare(double coordinateX,
                                  double coordinateY,
                                  double side,
                                  double newCoordinateX,
                                  double newCoordinateY,
                                  double coefficient) {
        Square square = new Square(coordinateX, coordinateY, side);
        square.printInfo();
        System.out.println(COORDINATES + "X = " + square.getCoordinateX() + "; Y = " + square.getCoordinateY());
        System.out.println(SIZE + "Сторона = " + square.getSideA());
        System.out.println(SQUARE + square.square());
        System.out.println(PERIMETER + square.perimeter());
        square.moveCenter(newCoordinateX, newCoordinateY);
        System.out.println(NEW_COORDINATES + "X = " + square.getCoordinateX() + "; Y = " + square.getCoordinateY());
        square.scaleFigure(coefficient);
        System.out.println(NEW_SIZE + "Сторона = " + square.getSideA());
        System.out.println(NEW_SQUARE + square.square());
        System.out.println(NEW_PERIMETER + square.perimeter());
    }
}
