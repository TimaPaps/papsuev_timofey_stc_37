/**
 * Class RemoteController - class TV control panel, establishes
 * connection with TV and indicates
 * which channel to broadcast
 *
 * @author Papsuev Timofey
 * @version v2.1
 */

public class RemoteController {
    private Tv tv;

    /**
     * @param tv link to install TV
     */
    public RemoteController(Tv tv) {
        this.tv = tv;
        this.tv.setRemoteController(this);
    }

    /**
     * Method - select a channel for showing on TV
     *
     * @param number channel for showing on tv
     */
    public void selectChannel(int number) {
        if (this.tv != null) {
            this.tv.showChannel(number);
        }
    }
}
