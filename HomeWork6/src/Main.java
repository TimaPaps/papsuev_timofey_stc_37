/**
 * Program model of the subject area "TV". Shows a random channel
 * on TV, the channel number on TV is transmitted from the control
 * panel
 *
 * @author Papsuev Timofey
 * @version v2.2
 */

public class Main {

    public static void main(String[] args) {
        ShowProgram.showProgram();
    }
}
