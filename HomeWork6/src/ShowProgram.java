import java.util.Scanner;

/**
 * 03.03.2021 9:03
 * HomeWork6
 *
 * Class - demonstration of the program
 *
 * @author Papsuev Timofey
 * @version v1.1
 */
public class ShowProgram {
    public static final int EXIT = (-1);
    static int numberCommand;

    static Scanner scannerInt = new Scanner(System.in);
    static Scanner scannerString = new Scanner(System.in);

    static Tv tv = new Tv();
    static RemoteController remoteController = new RemoteController(tv);
    static GeneratorChannelsAndPrograms generatorChannelsAndProgram = new GeneratorChannelsAndPrograms();

    /**
     * Method - demonstration of the program
     */
    public static void showProgram() {
        generatorChannelsAndProgram.generate(tv);

        while (true) {
            System.out.println("Введите номер предлагаемой команды или \"" + EXIT + "\" для остановки программы:\n" +
                    "1. Показать определенный канал.\n" +
                    "2. Добавить канал.\n" +
                    "3. Добавить программу в канал.\n" +
                    "4. Вывести список каналов.");

            while (!scannerInt.hasNextInt()) {
                System.err.println("Вы ввели не число, введите номер команды от 1 до 4");
                scannerInt.next();
            }

            numberCommand = scannerInt.nextInt();

            if (numberCommand == (EXIT)) {
                System.out.println("Программа остановлена!");
                break;
            } else if (numberCommand < 1 || numberCommand > 4) {
                System.err.println("Введенный номер: \"" + numberCommand + "\" - такой команды нет!!!");
                System.out.println();
                continue;
            }

            addCommandOne();
            addCommandTwo();
            addCommandThree();
            addCommandFour();
        }
    }

    /**
     * Method - add command № 1
     */
    private static void addCommandOne() {
        if (numberCommand == 1) {
            int numberShowChannel;

            while (true) {
                System.out.println("Введите номер канала от 0 до " + (tv.getChannels().length - 1) + " или \"" + EXIT +
                        "\" для выхода в Основное меню");

                while (!scannerInt.hasNextInt()) {
                    System.err.println("Вы ввели не число, введите номер канала от 0 до " +
                            (tv.getChannels().length - 1));
                    scannerInt.next();
                }

                numberShowChannel = scannerInt.nextInt();

                if (numberShowChannel == (EXIT)) {
                    System.out.println("Основное меню");
                    break;
                } else if (numberShowChannel < (EXIT) || numberShowChannel > (tv.getChannels().length - 1)) {
                    System.err.println("Введенный номер канала: \"" + numberShowChannel + "\"");
                    continue;
                } else {
                    if (tv.getChannels()[numberShowChannel].getPrograms().length == 0) {
                        System.err.println("Это новый канал, на нем еще нет ни одной программы, нужно добавить хотя " +
                                "бы одну программу");
                        break;
                    } else {
                        remoteController.selectChannel(numberShowChannel);
                    }
                }
            }
            System.out.println();
        }
    }

    /**
     * Method - add command № 2
     */
    private static void addCommandTwo() {
        if (numberCommand == 2) {
            String channelName;

            while (true) {
                System.out.println("Для добавления нового канала введите его название или \"" + EXIT +
                        "\" для выхода в Основное меню");

                channelName = scannerString.nextLine();

                if (channelName.equals("-1")) {
                    System.out.println("Основное меню");
                    break;
                } else if (channelName.equals("")) {
                    System.err.println("Вы не ввели название канала!");
                    continue;
                } else if (!channelName.equals("")) {
                    Channel channel = new Channel(channelName);
                    tv.addChannel(channel);
                    System.out.println("Обновленный список:");

                    for (int i = 0; i < tv.getChannels().length; i++) {
                        System.out.println(i + "." + tv.getChannels()[i].getChannelName());
                    }

                    System.out.println();
                }
            }
            System.out.println();
        }
    }

    /**
     * Method - add command № 3
     */
    private static void addCommandThree() {
        if (numberCommand == 3) {
            int numberChannel;
            String programName;

            while (true) {
                System.out.println("Для добавления программы в канал введите номер нужного канала или \"" + EXIT +
                        "\" для выхода в Основное меню");
                System.out.println("Доступные каналы:");

                for (int i = 0; i < tv.getChannels().length; i++) {
                    System.out.println(i + "." + tv.getChannels()[i].getChannelName());
                }

                while (!scannerInt.hasNextInt()) {
                    System.err.println("Вы ввели не число, введите номер канала от 0 до " +
                            (tv.getChannels().length - 1));
                    scannerInt.next();
                }

                numberChannel = scannerInt.nextInt();

                if (numberChannel == (EXIT)) {
                    System.out.println("Основное меню");
                    break;
                } else if (numberChannel < (EXIT) || numberChannel > (tv.getChannels().length - 1)) {
                    System.err.println("Введенный номер канала: \"" + numberChannel + "\"");
                    continue;
                } else {
                    System.out.println("Доступные программы:");

                    for (int i = 0; i < tv.getChannels()[numberChannel].getPrograms().length; i++) {
                        System.out.println(i + "." + tv.getChannels()[numberChannel].getPrograms()[i].getProgramName());
                    }

                    while (true) {
                        System.out.println("Введите название программы и нажмите Enter");
                        programName = scannerString.nextLine();

                        if (programName.equals("")) {
                            System.err.println("Вы не ввели название программы!");
                            continue;
                        } else {
                            Program program = new Program(programName);
                            tv.getChannels()[numberChannel].addProgram(program);
                            System.out.println("Обновленный список:");

                            for (int i = 0; i < tv.getChannels()[numberChannel].getPrograms().length; i++) {
                                System.out.println(i + "." +
                                        tv.getChannels()[numberChannel].getPrograms()[i].getProgramName());
                            }

                            System.out.println();
                            break;
                        }
                    }
                }
            }
            System.out.println();
        }
    }

    /**
     * Method - add command № 4
     */
    private static void addCommandFour() {
        if (numberCommand == 4) {
            System.out.println("Доступные каналы: ");

            for (int i = 0; i < tv.getChannels().length; i++) {
                System.out.println(i + "." + tv.getChannels()[i].getChannelName());
            }

            System.out.println();
        }
    }
}
