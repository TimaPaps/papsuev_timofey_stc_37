/**
 * Class Program
 *
 * @author Papsuev Timofey
 * @version v2.1
 */

public class Program {
    private String programName;

    /**
     * @param programName TV channel program name
     */
    public Program(String programName) {
        this.programName = programName;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }
}
