/**
 * Class TV - class shows a random channel program, the channel number is set
 * by the remote controller
 *
 * @author Papsuev Timofey
 * @version v2.1
 */

public class Tv {
    private RemoteController RemoteController;
    private Channel[] channels;

    /**
     * Constructor initializes an empty array
     */
    public Tv() {
        channels = new Channel[0];
    }

    public Channel[] getChannels() {
        return channels;
    }

    public void setChannels(Channel[] channels) {
        this.channels = channels;
    }

    /**
     * Method - connect the remote control to the TV
     *
     * @param RemoteController
     */
    public void setRemoteController(RemoteController RemoteController) {
        this.RemoteController = RemoteController;
    }

    /**
     * Method - show selected tv channel
     *
     * @param number channel for showing on tv
     */
    public void showChannel(int number) {
        if (number < 0 || number >= channels.length) {
            System.err.println("Канал под номером " + number +
                    " не существует");
        } else {
            System.out.println("На канале: "
                    + channels[number].getChannelName());
            channels[number].showProgram();
        }
    }

    /**
     * Method - add a new channel
     *
     * @param channel object Channel with channel name
     */
    public void addChannel(Channel channel) {
        Channel[] newArray = new Channel[channels.length + 1];

        for (int i = 0; i < channels.length; ++i) {
            newArray[i] = channels[i];
        }

        newArray[channels.length] = channel;
        this.channels = newArray;
    }

    /**
     * Method - add a new channel
     *
     * @param name string with channel name
     */
    public void addChannel(String name) {
        Channel[] newArray = new Channel[channels.length + 1];

        for (int i = 0; i < channels.length; ++i) {
            newArray[i] = channels[i];
        }

        newArray[channels.length] = new Channel(name);
        this.channels = newArray;
    }
}
