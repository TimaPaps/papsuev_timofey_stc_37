import java.util.Random;

/**
 * Class Channel - channel shows a random program on tv
 *
 * @author Papsuev Timofey
 * @version v2.1
 */

public class Channel {
    private String channelName;
    private Program[] programs;

    /**
     * Constructor initializes an empty array
     *
     * @param channelName TV channel name
     */
    public Channel(String channelName) {
        this.channelName = channelName;
        programs = new Program[0];
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public Program[] getPrograms() {
        return programs;
    }

    public void setPrograms(Program[] programs) {
        this.programs = programs;
    }

    /**
     * Method - show randomly channel program
     */
    public void showProgram() {
        Random random = new Random();
        int numberChannelRandom = random.nextInt(programs.length);
        System.out.println(programs[numberChannelRandom].getProgramName());
    }

    /**
     * Method - add a new program
     *
     * @param program object Program with program name
     */
    public void addProgram(Program program) {
        Program[] newArray = new Program[programs.length + 1];

        for (int i = 0; i < programs.length; ++i) {
            newArray[i] = programs[i];
        }

        newArray[programs.length] = program;
        this.programs = newArray;
    }

    /**
     * Method - add a new program
     *
     * @param name string with program name
     */
    public void addProgram(String name) {
        Program[] newArray = new Program[programs.length + 1];

        for (int i = 0; i < programs.length; ++i) {
            newArray[i] = programs[i];
        }

        newArray[programs.length] = new Program(name);
        this.programs = newArray;
    }
}
