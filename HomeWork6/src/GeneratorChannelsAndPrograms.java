/**
 * Class GeneratorChannelsAndPrograms - the class generates channels
 * and their programs
 *
 * @author Papsuev Timofey
 * @version v2.1
 */

public class GeneratorChannelsAndPrograms {
    public void generate(Tv tv) {
        String[] channelNames = {
                "Paramount Comedy",
                "Пятница",
                "Кинопоиск",
                "Техно 24",
                "Авто Плюс"
        };

        String[][] programNames = {
                {
                        "Котопес",
                        "Корпорация",
                        "Последний настоящий мужчина",
                        "Тусовщики",
                        "Друзья",
                        "Теория большого взрыва",
                        "Клиника",
                        "Старски и Хатч",
                        "Южный парк",
                        "Стендап от Paramount Comedy"
                },
                {
                        "Орел и решка",
                        "На ножах",
                        "Мир наизнанку",
                        "Адская кухня",
                        "Племя"
                },
                {
                        "Побег из Шоушенка",
                        "Зеленая миля",
                        "Интерстеллар",
                        "Форрест Гамп",
                        "Криминальное чтиво",
                        "Карты, деньги, два ствола",
                        "Начало",
                        "Джентельмены",
                        "Достучаться до небес",
                        "Леон"
                },
                {
                        "Гражданское оружие",
                        "Полигон",
                        "Секреты шефа",
                        "Искусство разведки",
                        "Охотники за караванами"
                },
                {
                        "Мега транспорт",
                        "Спорткары",
                        "Два колеса",
                        "Помешанные на скорости",
                        "Наши тесты"
                }
        };

        Channel[] channels = new Channel[channelNames.length];

        for (int i = 0; i < channels.length; i++) {
            channels[i] = new Channel(channelNames[i]);
            Program[] programs = new Program[programNames[i].length];

            for (int j = 0; j < programNames[i].length; j++) {
                programs[j] = new Program(programNames[i][j]);
            }

            channels[i].setPrograms(programs);
        }

        tv.setChannels(channels);
    }
}
