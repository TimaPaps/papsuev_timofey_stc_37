package ru.innojava.hashmap.classes;

import ru.innojava.hashmap.interfases.Map;

/**
 * 22.03.2021 19:15
 * HomeWork11
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class ShowProgram {
    Map<String, String> map = new HashMapImpl<>();

    public void showPut() {
        map.put("Tima", "Papsuev");
        map.put("Marsel", "Sidikov");
        map.put("Viktor", "Evlampev");
        map.put("Daniil", "Vdovinov");
        map.put("Djamil", "Sadikov");
        map.put("Nikolay", "Ponomarev");
        map.put("Siblings" , "Hello - 1");
        map.put("Teheran" , "Hello - 2");

        map.put("Tima", "Paps");
    }

    public void showGet() {
        System.out.println(map.get("Tima"));
        System.out.println(map.get("Daniil"));
        System.out.println(map.get("Marsel"));
        System.out.println(map.get("Viktor"));
        System.out.println(map.get("Nikolay"));
        System.out.println(map.get("Djamil"));
        System.out.println(map.get("Siblings"));
        System.out.println(map.get("Teheran"));
        System.out.println(map.get("Tra-ta-ta"));
    }
}
