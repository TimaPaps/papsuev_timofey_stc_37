package ru.innojava.hashmap.classes;

import ru.innojava.hashmap.interfases.Map;

/**
 * 22.03.2021 19:14
 * HomeWork11
 *
 * Класс - реализация ассоциативного массива
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class HashMapImpl<K, V> implements Map<K,V> {
    /**
     * Поле - константа
     */
    private static final int DEFAULT_SIZE = 16;
    /**
     * Массив - пары ключ-значение
     */
    private MapEntry<K, V>[] entries;

    public HashMapImpl() {
        this.entries = new MapEntry[DEFAULT_SIZE];
    }

    /**
     * Класс - возвращает объект ключ-значение
     * @param <K> - тип для ключа
     * @param <V> - тип для значения
     */
    private static class MapEntry<K, V> {
        /**
         * Поле - ключ
         */
        K key;
        /**
         * Поле - значение
         */
        V value;
        /**
         * Ссылка на следующий узел
         */
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    private int getIndex(K key) {
        int index = key.hashCode() & (entries.length - 1);
        return index;
    }

    /**
     * @param key - ключ для значения
     * @param value - значение, которое будет записано под определенным ключем
     */
    @Override
    public void put(K key, V value) {
        int index = getIndex(key);

        if (entries[index] != null) {
            MapEntry<K,V> current = entries[index];

            while (current != null) {

                if (current.key.equals(key)) {
                    current.value = value;
                    return;
                }
                current = current.next;
            }
            MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);
            newMapEntry.next = entries[index];
            entries[index] = newMapEntry;
        } else {
            entries[index] = new MapEntry<>(key, value);
        }
    }

    /**
     * @param key - ключ
     * @return - текущее значение по ключу
     */
    @Override
    public V get(K key) {
        // TODO: реализовать
        int index = getIndex(key);
        MapEntry<K, V> current = entries[index];

        while (current != null) {
            if (!current.key.equals(key)) {
                current = current.next;
            } else {
                return current.value;
            }
        }
        return null;
    }
}
