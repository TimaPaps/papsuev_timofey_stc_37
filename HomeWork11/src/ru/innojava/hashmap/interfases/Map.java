package ru.innojava.hashmap.interfases;

/**
 * 22.03.2021 19:12
 * HomeWork11
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface Map<K, V> {
    /**
     * Метод - записывает значение под определенным ключем
     *
     * @param key - ключ для значения
     * @param value - значение, которое будет записано под определенным ключем
     */
    void put(K key, V value);

    /**
     * Метод - позволяет получить значение по определенному ключу
     *
     * @param key - ключ
     * @return - значение полученное по определенному ключу
     */
    V get(K key);
}
