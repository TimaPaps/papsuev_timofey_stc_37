package ru.innojava.hashmap;

import ru.innojava.hashmap.classes.ShowProgram;

/**
 * 22.03.2021 19:11
 * HomeWork11
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        ShowProgram showProgram = new ShowProgram();
        showProgram.showPut();
        showProgram.showGet();
    }
}
