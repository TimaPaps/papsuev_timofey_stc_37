package ru.innojava.streams.app;

/**
 * 05.04.2021 11:08
 * HomeWork12
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        ShowProgram showProgram = new ShowProgram();
        showProgram.showProgram();
    }
}
