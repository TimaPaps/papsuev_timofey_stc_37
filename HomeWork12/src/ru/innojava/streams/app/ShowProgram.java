package ru.innojava.streams.app;

import ru.innojava.streams.repository.CarRepository;
import ru.innojava.streams.repository.CarRepositoryFileImpl;

/**
 * 06.04.2021 15:26
 * HomeWork12
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class ShowProgram {
    public void showProgram() {
        CarRepository carRepository = new CarRepositoryFileImpl();
        System.out.println("Полный список автомобилей, отсортирован по модели:");
        System.out.println(carRepository.findAll());
        System.out.println();
        System.out.println("Номера автомобилей черного цвета и/или с нулевым пробегом:");
        System.out.println(carRepository.findNumbersCarBlackOrZeroMileage());
        System.out.println();
        System.out.println("Количество уникальных моделей в ценовом диапазоне от 700000 до 800000:");
        System.out.println(carRepository.findAmountUniqueModelFrom700To800());
        System.out.println();
        System.out.println("Цвет автомобиля с минимальной стоимостью:");
        System.out.println(carRepository.findColorCarWithMinPrice());
        System.out.println();
        System.out.println("Средняя стоимость автомобилей Camry");
        System.out.println(carRepository.findAveragePriceCamry());
    }
}
