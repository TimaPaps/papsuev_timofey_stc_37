package ru.innojava.streams.repository;

import ru.innojava.streams.models.Car;

import java.util.List;

/**
 * 05.04.2021 11:29
 * HomeWork12
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface CarRepository {
    /**
     * Метод предоставляет список объектов типа Car (список автомобилей).
     *
     * @return - список типа Car.
     */
    List<Car> findAll();

    /**
     * Метод производит поиск объектов типа Car по двум условиям (цвет автомобиля или пробег).
     *
     * @return - список типа String (номера автомобилей).
     */
    List<String> findNumbersCarBlackOrZeroMileage();

    /**
     * Метод производит поиск уникальных объектов типа Car по условию (ценовой диапазон).
     *
     * @return - число (количество уникальных автомобилей).
     */
    Long findAmountUniqueModelFrom700To800();

    /**
     * Метод производит поиск объекта типа Car по условию (минимальная стоимость автомобиля).
     *
     * @return - строка (цвет автомобиля).
     */
    String findColorCarWithMinPrice();

    /**
     * Метод производит поиск объектов типа Car по условию (модель автомобиля).
     *
     * @return - число (средняя стоимость автомобилей определенной модели).
     */
    Double findAveragePriceCamry();
}
