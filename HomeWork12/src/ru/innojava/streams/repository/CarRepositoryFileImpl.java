package ru.innojava.streams.repository;

import ru.innojava.streams.models.Car;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 05.04.2021 14:09
 * HomeWork12
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class CarRepositoryFileImpl implements CarRepository {
    /**
     * Строковая константа, путь.
     */
    private static final String PATH_TO_AUTO = "cars.txt";

    /**
     * Функция, возвращает объект типа Car.
     */
    private final Function<String, Car> carMapper = line -> {
        if (line == null || line.isEmpty()) {
            return null;
        } else {
            line = line.substring(1, line.length() - 1);
            String[] parsedLine = line.split("]\\[");
            return new Car(parsedLine[0],
                    parsedLine[1],
                    parsedLine[2],
                    Integer.parseInt(parsedLine[3]),
                    Integer.parseInt(parsedLine[4]));
        }
    };

    /**
     * Метод, возвращает тип определенный в вызывающем методе.
     *
     * @param fileName - строка, путь.
     * @return - поток.
     */
    private <T> T common(String fileName, Function<BufferedReader, T> function) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return function.apply(reader);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<Car> findAll() {
        return common(PATH_TO_AUTO, reader ->
                reader.lines()
                        .map(carMapper)
                        .sorted(Comparator.comparing(Car::getModel))
                        .collect(Collectors.toList()));
    }

    @Override
    public List<String> findNumbersCarBlackOrZeroMileage() {
        return common(PATH_TO_AUTO, reader ->
                reader.lines()
                        .map(carMapper)
                        .filter(car -> car.getColor().equals("black") || car.getMileage() == 0)
                        .map(Car::getNumber)
                        .collect(Collectors.toList()));
    }

    @Override
    public Long findAmountUniqueModelFrom700To800() {
        return common(PATH_TO_AUTO, reader ->
                reader.lines()
                        .map(carMapper)
                        .filter(car -> car.getPrice() >= 700000 && car.getPrice() <= 800000)
                        .map(Car::getModel)
                        .distinct()
                        .count());
    }

    @Override
    public String findColorCarWithMinPrice() {
        return common(PATH_TO_AUTO, reader ->
                reader.lines()
                        .map(carMapper)
                        .min(Comparator.comparing(Car::getPrice))
                        .map(Car::getColor)
                        .orElse("Что- то пошло не так, возможно список пуст"));
    }

    @Override
    public Double findAveragePriceCamry() {
        return common(PATH_TO_AUTO, reader ->
                reader.lines()
                        .map(carMapper)
                        .filter(car -> car.getModel().equals("camry"))
                        .mapToInt(Car::getPrice)
                        .average()
                        .orElse(0.0));
    }
}
