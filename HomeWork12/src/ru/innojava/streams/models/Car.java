package ru.innojava.streams.models;

import java.util.Objects;

/**
 * 05.04.2021 11:10
 * HomeWork12
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Car {
    /**
     * Строка, модель автомобиля.
     */
    private final String model;
    /**
     * Строка, цвет автомобиля.
     */
    private String color;
    /**
     * Число, пробег автомобиля.
     */
    private Integer mileage;
    /**
     * Число, стоимость автомобиля.
     */
    private Integer price;
    /**
     * Число, регистрационный номер автомобиля.
     */
    private String number;

    public Car(String number, String model, String color, Integer mileage, Integer price) {
        this.model = model;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
        this.number = number;
    }


    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return number == car.number
                && Objects.equals(model, car.model)
                && Objects.equals(color, car.color)
                && Objects.equals(mileage, car.mileage)
                && Objects.equals(price, car.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(model, color, mileage, price, number);
    }

    @Override
    public String toString() {
        return "number = " + number
                + " model = '" + model + "'"
                + " color = '" + color + "'"
                + " mileage = " + mileage
                + " price = " + price + "\n";
    }
}
