package ru.innojava.lambdas.classes;

/**
 * 09.03.2021 15:59
 * HomeWork9
 * <p>
 * Класс демонстрации работы классов, интерфейсов и их лямбда-реализаций
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class ShowProgram {
    public static void showNumbers() {
        NumbersUtil numbersUtil = new NumbersUtil();

        // разворот числа
        numbersUtil.process(4506, number -> {
            int temp;
            int reversNumber = 0;
            while (number > 0) {
                temp = number % 10;
                number /= 10;
                reversNumber *= 10;
                reversNumber += temp;
            }
            return reversNumber;
        });

        // убрать нули из исходного числа
        numbersUtil.process(40570080, number -> {
            int temp;
            int resultNumber = 0;
            int reversNumber = 0;

            while (number > 0) {
                temp = number % 10;
                number /= 10;

                if (temp != 0) {
                    reversNumber *= 10;
                    reversNumber += temp;
                }
            }

            while (reversNumber > 0) {
                temp = reversNumber % 10;
                reversNumber /= 10;
                resultNumber *= 10;
                resultNumber += temp;
            }
            return resultNumber;
        });

        // заменить нечетные цифры ближайшей четной снизу
        numbersUtil.process(45277, number -> {
            int tempOne;
            int numberTemp;
            int resultNumber = 0;
            int reversNumber = 0;
            int tempTwo = 0;

            while (number > 0) {
                tempOne = number % 10;
                numberTemp = number;

                if (tempOne % 2 == 0) {
                    tempTwo = tempOne;
                } else {
                    while (numberTemp % 2 != 0) {
                        numberTemp /= 10;
                        tempOne = numberTemp % 10;

                        if (tempOne % 2 == 0) {
                            tempTwo = tempOne;
                        }
                    }
                }

                number /= 10;
                reversNumber *= 10;
                reversNumber += tempTwo;
            }

            while (reversNumber > 0) {
                tempOne = reversNumber % 10;
                reversNumber /= 10;
                resultNumber *= 10;
                resultNumber += tempOne;
            }
            return resultNumber;
        });

        numbersUtil.showProcessed();
    }

    public static void showString() {
        StringUtil stringUtil = new StringUtil();

        stringUtil.process("Hello World!", string -> {
            String resultString = "";
            char[] array = string.toCharArray();

            for (int i = array.length - 1; i >= 0; --i) {
                resultString += array[i];
            }
            return resultString;
        });

        stringUtil.process("Hel00lo61132 1Wor450998ld!", string -> {
            String resultString = "";
            char[] array = string.toCharArray();

            for (int i = 0; i < array.length; ++i) {
                if (!Character.isDigit(array[i])) {
                    resultString += array[i];
                }
            }
            return resultString;
        });

        stringUtil.process("Hello World!", string -> string.toUpperCase());

        stringUtil.showProcessed();
    }

    public static void showArray() {
        int[] numbers = {5, 45, 6, 7};
        String[] strings = {"Hello World!", "Tima Papsuev"};

        NumbersAndStringsProcessor processor = new NumbersAndStringsProcessor(numbers, strings);

        int[] processedNumbers = processor.processNumbers(number -> number += 5);

        String[] processedStrings = processor.processStrings(string -> string.toUpperCase());

        processor.showArrayNumbers(processedNumbers);
        processor.showArrayStrings(processedStrings);
    }
}
