package ru.innojava.lambdas.classes;

import ru.innojava.lambdas.interfaces.NumbersProcess;

/**
 * 04.03.2021 20:52
 * HomeWork9
 *
 * Класс - преобразует число/числа
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class NumbersUtil {
    /**
     * Счетчик
     */
    private int processedNumbersCount;
    /**
     * Массив - хранит преобразованное число/числа
     */
    private int[] processedNumbers = new int[10];

    /**
     * Метод - преобразует число в зависимости от реализации описанной в функции
     *
     * @param number - исходное число
     * @param function - функция, реализующая преобразование числа
     */
    public void process(int number, NumbersProcess function) {
        int processedNumber = function.process(number);

        saveNumber(processedNumber);
    }

    /**
     * Метод - записывает преобразованное число в массив, реализована возможность расширения массива
     *
     * @param number - преобразованное число
     */
    private void saveNumber(int number) {
        if (processedNumbersCount < processedNumbers.length) {
            processedNumbers[processedNumbersCount] = number;
        } else {
            int[] tempArray = new int[processedNumbers.length + 1];

            for (int i = 0; i < processedNumbers.length; ++i) {
                tempArray[i] = processedNumbers[i];
            }
            tempArray[processedNumbers.length] = number;
            this.processedNumbers = tempArray;
        }
        processedNumbersCount++;
    }

    /**
     * Метод - выводит число/числа из массива преобразованных чисел
     */
    public void showProcessed() {
        for (int i = 0; i < processedNumbersCount; ++i) {
            System.out.println(processedNumbers[i]);
        }
    }
}

