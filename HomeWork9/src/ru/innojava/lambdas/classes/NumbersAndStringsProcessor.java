package ru.innojava.lambdas.classes;

import ru.innojava.lambdas.interfaces.NumbersProcess;
import ru.innojava.lambdas.interfaces.StringProcess;

/**
 * 04.03.2021 18:38
 * HomeWork9
 * <p>
 * Класс - преобразует массив чисел и массив строк
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class NumbersAndStringsProcessor {
    /**
     * Массив - хранит исходные числа
     */
    private int[] numbers;
    /**
     * Массив - хранит исходные строки
     */
    private String[] strings;

    public NumbersAndStringsProcessor(int[] numbers, String[] strings) {
        this.numbers = numbers;
        this.strings = strings;
    }

    public NumbersAndStringsProcessor(int[] numbers) {
        this.numbers = numbers;
    }

    public NumbersAndStringsProcessor(String[] strings) {
        this.strings = strings;
    }

    /**
     * Метод - преобразует массив чисел в зависимости от реализации описанной в функции
     *
     * @param function - реализует преобразование числа
     * @return - массив преобразованных чисел
     */
    public int[] processNumbers(NumbersProcess function) {
        int[] result = new int[numbers.length];

        for (int i = 0; i < result.length; i++) {
            result[i] = function.process(numbers[i]);
        }
        return result;
    }

    /**
     * Метод - преобразует массив строк в зависимости от реализации описанной в функции
     *
     * @param function - реализует преобразование строки
     * @return - массив преобразованных строк
     */
    public String[] processStrings(StringProcess function) {
        String[] result = new String[strings.length];

        for (int i = 0; i < strings.length; i++) {
            result[i] = function.process(strings[i]);
        }
        return result;
    }

    /**
     * Метод - выводит строки из массива преобразованных строк
     */
    public void showArrayStrings(String[] strings) {
        for (int i = 0; i < strings.length; i++) {
            System.out.print(strings[i] + " ");
        }
        System.out.println();
    }

    /**
     * Метод - выводит числа из массива преобразованных чисел
     */
    public void showArrayNumbers(int[] numbers) {
        for (int i = 0; i < numbers.length; ++i) {
            System.out.print(numbers[i] + " ");
        }
        System.out.println();
    }
}
