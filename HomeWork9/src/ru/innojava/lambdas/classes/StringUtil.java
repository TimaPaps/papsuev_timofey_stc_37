package ru.innojava.lambdas.classes;

import ru.innojava.lambdas.interfaces.StringProcess;

/**
 * 04.03.2021 20:52
 * HomeWork9
 *
 * Класс - преобразует строку/строки
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class StringUtil {
    /**
     * Счетчик
     */
    private int processedStringCount;
    /**
     * Массив - хранит преобразованную строку/строки
     */
    private String[] processedString = new String[10];

    /**
     * Метод - преобразует строку в зависимости от реализации описанной в функции
     *
     * @param string - исходная строка
     * @param function - функция, реализующая преобразование строки
     */
    public void process(String string, StringProcess function) {
        String processedString = function.process(string);

        saveString(processedString);
    }

    /**
     * Метод - записывает преобразованную строку в массив, реализована возможность расширения массива
     *
     * @param string - преобразованная строка
     */
    private void saveString(String string) {
        if (processedStringCount < processedString.length) {
            processedString[processedStringCount] = string;
        } else {
            String[] tempArray = new String[processedString.length + 1];

            for (int i = 0; i < processedString.length; ++i) {
                tempArray[i] = processedString[i];
            }
            tempArray[processedString.length] = string;
            this.processedString = tempArray;
        }
        processedStringCount++;
    }

    /**
     * Метод - выводит строку/строки из массива преобразованных строк
     */
    public void showProcessed() {
        for (int i = 0; i < processedStringCount; ++i) {
            System.out.println(processedString[i]);
        }
    }
}
