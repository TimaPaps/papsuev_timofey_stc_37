package ru.innojava.lambdas.interfaces;

/**
 * 04.03.2021 18:37
 * HomeWork9
 *
 * Функциональный интерфейс
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface StringProcess {
    /**
     * @param string - строка для преобразования
     * @return - преобразованная строка
     */
    String process(String string);
}
