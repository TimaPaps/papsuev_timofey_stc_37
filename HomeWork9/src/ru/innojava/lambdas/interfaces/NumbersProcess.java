package ru.innojava.lambdas.interfaces;

/**
 * 04.03.2021 18:33
 * HomeWork9
 *
 * Функциональный интерфейс
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface NumbersProcess {
    /**
     * @param number - число для преобразования
     * @return - преобразованное число
     */
    int process(int number);
}
