import ru.innojava.lambdas.classes.ShowProgram;

/**
 * 04.03.2021 18:33
 * HomeWork9
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Main {

    public static void main(String[] args) {
        ShowProgram.showNumbers();
        ShowProgram.showString();
        ShowProgram.showArray();
    }
}
