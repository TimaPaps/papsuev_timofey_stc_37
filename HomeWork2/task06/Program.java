/**
 *Приложение, которое выполняет преобразование массива в число.
 *
 *@autor Timofey Papsuev
 *@version 1.0
 *
 *Программа работает с заданным массивом целых чисел
 */

class Program {
	public static void main(String[] args) {
		int array[] = {4, 2, 3, 5, 7};
		int number = 0;

		String stringFromArray = "";

		for (int i = 0; i < array.length; i++) {
			stringFromArray = stringFromArray + array[i];	
		}

		number = Integer.parseInt(stringFromArray);

		System.out.println(number);
	}
}