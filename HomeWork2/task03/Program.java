import java.util.Scanner;
//import java.util.Arrays;									//для отладки

/**
 *Приложение, которое вычисляет среднее арифметическое элемментов 
 *массива (масссив вводится с клавиатуры - по одному числу в каждой
 *новой строке)
 *
 *@autor Timofey Papsuev
 *@version 1.0
 *
 *Программа работает с целыми числами
 */

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter integer number > 0");

		int n = scanner.nextInt();

		if (n <= 0) {
			System.out.println("Entered number < or = 0");
			return;
		}

		System.out.println();
		
		int array[] = new int[n];
		double arrayAverage = 0;

		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
			//System.out.println(Arrays.toString(array));	//для отладки
			arrayAverage = arrayAverage + array[i];
		}

		arrayAverage = arrayAverage / array.length;

		System.out.println();
		System.out.println(arrayAverage);
	}
}