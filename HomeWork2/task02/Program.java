import java.util.Scanner;
import java.util.Arrays;

/**
 *Приложение, которое выполняет разворот массива (массив вводится с
 *клавиатуры - по одному числу с каждой новой строки)
 *
 *@autor Timofey Papsuev
 *@version 1.0
 *
 *Программа работает с целыми положительными числами
 */

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter integer number > 0");

		int n = scanner.nextInt();

		if (n <= 0) {
			System.out.println("Entered number < or = 0");
			return;
		}

		System.out.println();

		int array[] = new int[n];

		//второй вариант (появился после первого)
//		for (int i = array.length - 1; i >= 0; i--) {
//			array[i] = scanner.nextInt();
//		}

//		System.out.println();
//		System.out.println(Arrays.toString(array));


		//первый вариант
		int arrayMirror[] = new int[array.length];
		int j = array.length - 1;

		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();

			arrayMirror[j] = array[i];
			j--;

		//System.out.println(Arrays.toString(arrayMirror));	//для отладки
		}
		array = null;

		//System.out.println(Arrays.toString(array));		//для отладки
		System.out.println();
		System.out.println(Arrays.toString(arrayMirror));
	}
}