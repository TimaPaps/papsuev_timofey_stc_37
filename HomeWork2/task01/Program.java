import java.util.Scanner;
//import java.util.Arrays;									//для отладки

/**
 *Приложение, которое выводит сумму элементов массива (масссив
 *вводится с клавиатуры - по одному числу в каждой новой строке)
 *
 *@autor Timofey Papsuev
 *@version 1.0
 *
 *Программа работает с целыми числами
 */

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter integer number > 0");

		int n = scanner.nextInt();

		if (n <= 0) {
			System.out.println("Entered number < or = 0");
			return;
		}

		System.out.println();

		int[] array = new int[n];
		int arraySum = 0;

		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
			arraySum = arraySum + array[i];	

			//System.out.println(Arrays.toString(array));	//для отладки
		}

		System.out.println();
		System.out.println(arraySum);
	}
}