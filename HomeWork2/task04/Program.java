import java.util.Scanner;
import java.util.Arrays;									

/**
 *Приложение, которое меняет местами максимальный и минимальный
 *элементы массива (масссив вводится с клавиатуры - по одному числу
 *в каждой новой строке)
 *
 *@autor Timofey Papsuev
 *@version 1.0
 *
 *Программа работает с целыми числами
 */

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter integer number > 0");

		int n = scanner.nextInt();

		if (n <= 0) {
			System.out.println("Entered number < or = 0");
			return;
		}

		System.out.println();

		int array[] = new int[n];

		//System.out.println(Arrays.toString(array));	//для отладки	

		int min = 0;
		int max = 0;
		int minPosition = -1;
		int maxPosition = -1;

		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();

			if (i == 0) {
				min = array[0];
				max = array[0];
			}

			if (array[i] <= min) {
				min = array[i];
				minPosition = i;
			}

			if (array[i] >= max) {
				max = array[i];
				maxPosition = i;
			}
		}

		//System.out.println(Arrays.toString(array));	//для отладки	
		if (minPosition >= 0 && maxPosition >= 0) {
			array[minPosition] = max;
			array[maxPosition] = min;
		}

		System.out.println();
		System.out.println(Arrays.toString(array));
	}
}