import java.util.Scanner;
import java.util.Arrays;									

/**
 *Приложение, которое заполняет двумерный массив M*N 
 *последовательностью чисел "по спирали".
 *
 *@autor Timofey Papsuev
 *@version 1.0
 */

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter two integer numbers > 0");

		int m = scanner.nextInt();
		int n = scanner.nextInt();
		int number = 1;

		if (m <= 0 || n <= 0) {
			System.out.println("One or both numbers < or = 0");
			return;
		}

		/*
		объявление и инициализация двумерного массива размером m-строк
		на n-столбцов.
		*/
		int[][] twoDimensionalArray = new int[m][n];

		/*
		заполнение периметра массива по часовой стрелке
		*/
		//направо
		for (int x = 0; x < n; x++) {
			twoDimensionalArray[0][x] = number;
			number++;
		} 

		//вниз
		for (int y = 1; y < m; y++) {
			twoDimensionalArray[y][n - 1] = number;
			number++;
		}

		//налево
		for (int x = n - 2; x >= 0; x--) {
			twoDimensionalArray[m - 1][x] = number;
			number++;
		}

		//вверх
		for (int y = m - 2; y > 0; y--) {
			twoDimensionalArray[y][0] = number;
			number++;
		}

		/*
		заполнение массива внутри периметра
		*/
		int x = 1;
		int y = 1;

		while (number < m * n) {

			//направо
			while (twoDimensionalArray[y][x + 1] == 0) {
				twoDimensionalArray[y][x] = number;
				number++;
				x++;
			}

			//вниз
			while (twoDimensionalArray[y + 1][x] == 0) {
				twoDimensionalArray[y][x] = number;
				number++;
				y++;
			}

			//влево
			while (twoDimensionalArray[y][x - 1] == 0) {
				twoDimensionalArray[y][x] = number;
				number++;
				x--;
			}

			//вверх
			while (twoDimensionalArray[y - 1][x] == 0) {
				twoDimensionalArray[y][x] = number;
				number++;
				y--;
			}
		}

		//заполнение последнего элемента
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (twoDimensionalArray[i][j] == 0) {
					twoDimensionalArray[i][j] = number;
				}
			}
		}

		//вывод в консоль
		for (int i = 0; i < twoDimensionalArray.length; i++) {
			for (int j = 0; j < twoDimensionalArray[i].length; j++) {
				if (twoDimensionalArray[i][j] < 10) {
					System.out.print(" " + twoDimensionalArray[i][j] + "  ");	
				} else {
					System.out.print(" " + twoDimensionalArray[i][j] + " ");
				}
			}
			System.out.println();
		}
		
		//System.out.println(Arrays.deepToString(twoDimensionalArray));
	}
}