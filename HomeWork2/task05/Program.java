import java.util.Scanner;
import java.util.Arrays;									

/**
 *Приложение, которое выполняет сортировку массива методом пузырька.
 *
 *@autor Timofey Papsuev
 *@version 1.0
 *
 *Программа работает с целыми числами
 */

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter integer number > 0");

		int n = scanner.nextInt();

		if (n <= 0) {
			System.out.println("Entered number < or = 0");
			return;
		}

		System.out.println();

		int array[] = new int[n];

		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}

		System.out.println(Arrays.toString(array));

		//сортировка
		boolean isSorted = false;
		int temp;

		while (isSorted == false) {
			isSorted = true;
			for (int i = 0; i < array.length - 1; i++) {
				if (array[i] > array[i + 1]) {
					isSorted = false;

					temp = array[i];
					array[i] = array[i + 1];
					array[i + 1] = temp;
				}
			}
		}

		System.out.println();
		System.out.println(Arrays.toString(array));
	}
}